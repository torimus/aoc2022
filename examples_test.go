package main

import (
	"aoc/lib"
	"fmt"
	"log"
	"os"
	"testing"
)

// Type of a testing function
type testFn[T comparable] func(*lib.Input) (T, T)

func TestDay1(t *testing.T) {
	exampleTest(t, 1, day1, 24000, 45000)
}

func TestDay2(t *testing.T) {
	exampleTest(t, 2, day2, 15, 12)
}

func TestDay3(t *testing.T) {
	exampleTest(t, 3, day3, 157, 70)
}

func TestDay4(t *testing.T) {
	exampleTest(t, 4, day4, 2, 4)
}

func TestDay5(t *testing.T) {
	exampleTest(t, 5, day5, "CMZ", "MCD")
}

func TestDay6(t *testing.T) {
	exampleTest(t, 6, day6, 7, 19)
}

func exampleTest[R comparable](t *testing.T, day uint, dayFn testFn[R], res1, res2 R) {
	input := exampleInput(day)
	if r1, r2 := dayFn(&input); r1 != res1 || r2 != res2 {
		t.Errorf("Expected %#v, got %#v !", []R{res1, res2}, []R{r1, r2})
	}
}

func exampleInput(day uint) lib.Input {
	con, err := os.ReadFile(fmt.Sprintf("data/day%03d_example.txt", day))
	if err != nil {
		log.Fatalln(err)
	}
	return lib.Lines(con)
}

//  vim: set ft=go ts=4 sts=4 sw=4 tw=0 fdm=syntax:
