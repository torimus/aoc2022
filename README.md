### Solutions for [Advent of Code](https://adventofcode.com/) programming puzles, written in [Go](https://go.dev/) programming language.

For year 2022 the Go language has been chosen. Previous years were solved in Ruby, Haskell, Python and PHP.

Let me know if you can point to a more elegant, concise or idiomatic way, how a particular puzzle has been solved in Go.
It is assumed, it has to be within boundaries of the standard library and [Go's experimental packages](https://pkg.go.dev/golang.org/x/exp) utmost.

If you are willing to share a better solution, fill in an [issue request](https://gitlab.com/torimus/aoc2022/-/issues) related to this repo.

Thanks for your understanding.

Take care.
