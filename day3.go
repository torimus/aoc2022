package main

import (
	"aoc/lib"

	"golang.org/x/exp/maps"
)

type (
	Set = lib.Set[rune]
)

func day3(inp *lib.Input) (prio1, prio2 int) {
	var triplet = make([]Set, 0)
	for _, rsack := range *inp {
		// part 1
		com1 := lib.NewSet([]rune(rsack[:len(rsack)/2]))
		if st := lib.NewSet([]rune(rsack[len(rsack)/2:])).
			Intersect(&com1); len(st) > 0 {
			prio1 += calcPrio(maps.Keys(st)[0])
		}
		// part 2
		triplet = append(triplet, lib.NewSet([]rune(rsack)))
		if len(triplet) == 3 {
			st1 := triplet[0].Intersect(&triplet[1])
			st2 := triplet[1].Intersect(&triplet[2])
			if len(st1) > 0 && len(st2) > 0 {
				if st3 := st1.Intersect(&st2); len(st3) > 0 {
					prio2 += calcPrio(maps.Keys(st3)[0])
				}
			}
			triplet = triplet[:0]
		}
	}

	return prio1, prio2
}

func calcPrio(c rune) int {
	if c >= 'a' {
		return int(c-'a') + 1
	} else {
		return int(c-'A') + 27
	}
}
