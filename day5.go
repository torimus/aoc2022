/* Copyright (C) 2022  Torimus Blake
 *
 * Author: Torimus Blake <no.public@email.sorry>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"aoc/lib"
	"regexp"
	"strconv"
)

func day5(inp *lib.Input) (res1, res2 string) {
	type (
		Instruction struct {
			amount, from, to int
		}
	)
	var (
		stackRx = regexp.MustCompile(`\d+`)
		stacks1 = make([]lib.Stack[rune], 0)
		stacks2 = make([]lib.Stack[rune], 0)
		moveRx  = regexp.MustCompile(
			`move\s+(?P<amount>\d+)\s+from\s+(?P<from>\d+)\s+to\s+(?P<to>\d+)`)
		amIx = moveRx.SubexpIndex("amount")
		frIx = moveRx.SubexpIndex("from")
		toIx = moveRx.SubexpIndex("to")
		lnum int
		row  string
		rows = make([]string, 0)
	)

	for lnum, row = range *inp {
		if row == "" {
			lnum--
			break
		}
		rows = append(rows, row)
	}
	// create stacks holding crates (preceding line of empty one)
	for r, match := range stackRx.FindAllStringSubmatchIndex(rows[lnum], -1) {
		stacks1 = append(stacks1, make(lib.Stack[rune], 0))
		// fill stacks with crate symbols
		for _, row := range rows[:lnum] {
			crate := rune(row[match[0]])
			ok, _ := regexp.MatchString(`[[:upper:]]`, string(crate))
			if ok {
				stacks1[r].Insert(crate, 0)
			}
		}
	}
	for _, st := range stacks1 {
		// clone stack compound element (slice)
		stc := append(make(lib.Stack[rune], 0), st...)
		stacks2 = append(stacks2, stc)
	}

	// perform move instructions
	for _, row := range (*inp)[lnum+2:] {
		if row == "" {
			break
		}
		for _, ins := range moveRx.FindAllStringSubmatch(row, -1) {
			fr, to := μ(strconv.Atoi(ins[frIx])), μ(strconv.Atoi(ins[toIx]))
			cnt := μ(strconv.Atoi(ins[amIx]))
			// part 1
			for c := cnt; c > 0; c-- {
				val, _ := stacks1[fr-1].Pop()
				stacks1[to-1].Push(val)
			}
			// part 2
			slIx := len(stacks2[fr-1]) - cnt
			stacks2[to-1] = append(stacks2[to-1], stacks2[fr-1][slIx:]...)
			stacks2[fr-1] = stacks2[fr-1][:slIx]
		}
	}

	// collect tops as result
	for _, stk := range stacks1 {
		if val, err := stk.Pop(); err == nil {
			res1 += string(val)
		}
	}
	for _, stk := range stacks2 {
		if val, err := stk.Pop(); err == nil {
			res2 += string(val)
		}
	}

	return res1, res2
}

//  vim: set ft=go ts=4 sts=4 sw=4 tw=0 fdm=syntax:
