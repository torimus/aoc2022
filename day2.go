package main

import (
	"aoc/lib"
	"log"
	str "strings"
)

type (
	Shape    int
	Direct   struct{}
	Reactive struct{}
	Strategy interface {
		Direct | Reactive
	}
)

const (
	Rock Shape = 1 + iota
	Paper
	Scissors
)

// Calculate round's outcome based on a given strategy
func outcome[T Strategy](s1, s2 Shape) (res int) {
	var strat T
	switch any(strat).(type) {
	case Direct:
		if res = (int(s2-s1) + 1) % 3; res < 0 {
			res = res + 3
		}
		res = res*3 + int(s2)
	case Reactive:
		s2 = Shape(int(s1+s2)%3 + 1)
		res = outcome[Direct](s1, s2)
	default:
		log.Fatalf("Unsupported strategy: %T!\n", strat)
	}
	return
}

func day2(inp *lib.Input) (score1, score2 int) {
	var pl1Shape = map[string]Shape{"A": Rock, "B": Paper, "C": Scissors}
	var pl2Shape = map[string]Shape{"X": Rock, "Y": Paper, "Z": Scissors}

	for _, line := range *inp {
		round := str.Split(str.TrimSpace(line), " ")
		pl1, ok1 := pl1Shape[round[0]]
		pl2, ok2 := pl2Shape[round[1]]
		if !(ok1 && ok2) {
			log.Fatalln("Invalid input!", round)
		}
		score1 += outcome[Direct](pl1, pl2)
		score2 += outcome[Reactive](pl1, pl2)
	}

	return score1, score2
}
