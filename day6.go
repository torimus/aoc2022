/* Copyright (C) 2022  Torimus Blake
 *
 * Author: Torimus Blake <no.public@email.sorry>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"aoc/lib"
	"strings"
)

func day6(inp *lib.Input) (res1, res2 int) {
	for _, row := range *inp {
		if strings.TrimSpace(row) != "" {
			if ix, ok := detectUnique([]rune(row), 4); ok {
				res1 = ix + 4
			}
			if ix, ok := detectUnique([]rune(row), 14); ok {
				res2 = ix + 14
			}
		}
	}
	return
}

// Returns index of unique seqyence of a given length,
func detectUnique[T comparable](sl []T, c int) (ix int, ok bool) {
	for ix = 0; ix+c <= len(sl); ix++ {
		if len(lib.NewSet(sl[ix:ix+c])) == c {
			ok = true
			break
		}
	}
	return
}
