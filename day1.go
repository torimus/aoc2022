package main

import (
	"aoc/lib"
	"log"
	"sort"
	"strconv"
	str "strings"
)

func day1(inp *lib.Input) (max, top int) {
	var (
		curr int
		top3 [3]int
	)

	for row, cal := range *inp {
		switch {
		case str.TrimSpace(cal) != "":
			i, err := strconv.Atoi(cal)
			if err != nil {
				log.Fatalln(err)
			}
			curr += i
			if row+1 < len(*inp) {
				break
			}
			fallthrough
		default:
			high := append(top3[:], curr)
			sort.Ints(high)
			copy(top3[:], high[len(high)-3:])
			curr = 0
		}
	}
	max = top3[2]
	top = sum(top3[:])

	return
}
