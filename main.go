/* Copyright (C) 2022  Torimus Blake
 *
 * Author: Torimus Blake <no.public@email.sorry>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"aoc/lib"

	cns "golang.org/x/exp/constraints"
)

var (
	μ = lib.Μ[int]
)

func init() {
	log.SetPrefix("» ")
	log.SetFlags(log.Lshortfile)
}

func main() {
	os.Args[0] = "Advent of Code programming puzzles solutions"
	day := flag.Uint("d", 0, "day number to compute solution for")
	flag.Parse()
	if *day == 0 {
		flag.Usage()
		return
	}

	fmt.Println(solve(*day))
}

func solve(day uint) (res1, res2 any) {
	var inp = lib.GetInput(day)
	switch day {
	case 1:
		res1, res2 = day1(&inp)
	case 2:
		res1, res2 = day2(&inp)
	case 3:
		res1, res2 = day3(&inp)
	case 4:
		res1, res2 = day4(&inp)
	case 5:
		res1, res2 = day5(&inp)
	case 6:
		res1, res2 = day6(&inp)
	default:
		res1, res2 = fmt.Sprintf("Day %d not implemented!", day), ""
	}
	return
}

// Sum numeric values of a slice
func sum[T cns.Integer](sl []T) T {
	return lib.Reduce(&sl, func(ac T, e T) T { return ac + e })
}
