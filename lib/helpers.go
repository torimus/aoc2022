package lib

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type (
	// input data
	Input []string
	// Unique elements connection
	Set[T comparable] map[T]struct{}
	// linear collection
	Stack[T any] []T
)

const (
	inputURI  = "https://adventofcode.com/2022/day/%d/input"
	cookieEnv = "AOC_COOKIE"
)

var (
	cachePath string // project's subdirectory where input data reside
	// cookie is by default set from AOC_COOKIE environment variable at initialization
	cookie string // cookie session hash retrieved from successful login
)

func init() {
	log.SetFlags(log.Lshortfile)
	path, err := os.Executable()
	if err != nil {
		log.Fatalln(err)
	}
	path = filepath.Dir(path)
	path, _ = os.Getwd() // for `go run' only, comment out for executable
	cachePath = filepath.Join(path, "data")
}

func GetInput(day uint) Input {
	if inp := cacheLoad(day); inp != nil {
		return inp
	}

	var ok bool
	if cookie, ok = os.LookupEnv(cookieEnv); !ok {
		log.Fatalf("Environment variable`%s' with session cookie for `adventofcode.com' site is not set!", cookieEnv)
	}
	req, err := http.NewRequest("GET", fmt.Sprintf(inputURI, day), nil)
	if err != nil {
		log.Fatalln(err)
	}
	cookie := http.Cookie{Name: "session", Value: cookie}
	req.AddCookie(&cookie)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	con, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	cacheStore(day, con)

	return Lines(con)
}

// Apply mapping function on each element of a slice.
func Map[F, T any](sl *[]F, fn func(F) T) []T {
	res := make([]T, len(*sl))
	for i, v := range *sl {
		res[i] = fn(v)
	}
	return res
}

// Apply reduction function on each element of a slice.
func Reduce[F, T any](sl *[]F, fn func(T, F) T) (res T) {
	for _, val := range *sl {
		res = fn(res, val)
	}
	return
}

// Create new Set instance from a slice
func NewSet[T comparable](sl []T) Set[T] {
	var res = make(Set[T])
	for _, v := range sl {
		res[v] = struct{}{}
	}
	return res
}

// Find intersection between Sets
func (s1 Set[T]) Intersect(s2 *Set[T]) Set[T] {
	res := make(Set[T])
	for c, _ := range *s2 {
		if _, ok := s1[c]; ok {
			res[c] = struct{}{}
		}
	}
	return res
}

// Stack-handling methods implementation
func (st *Stack[T]) Insert(val T, ix int) error {
	var placeholder T
	if ix > len(*st) {
		return errors.New(fmt.Sprintf("Index out of range %d!", ix))
	}
	*st = append(*st, placeholder)
	copy((*st)[ix+1:], (*st)[ix:])
	(*st)[ix] = val
	return nil
}
func (st *Stack[T]) Push(val T) {
	st.Insert(val, len(*st))
}
func (st *Stack[T]) Pop() (res T, err error) {
	var sl = len(*st)
	if sl == 0 {
		return res, errors.New("Stack is empty!")
	}
	res, *st = (*st)[sl-1], (*st)[:sl-1]
	return res, nil
}

// Split a byteslice into input strings, broken by newline character
func Lines(src []byte) Input {
	return strings.Split(string(src), "\n")
}

// Get first from multivalue argument
func Μ[T any](val ...any) T {
	res, _ := val[0].(T)
	return res
}

// Load cached input data for a given day
func cacheLoad(day uint) Input {
	if con, err := os.ReadFile(dataFile(day)); err == nil {
		return Lines(con)
	}
	return nil
}

// Store input data for a given day
func cacheStore(day uint, data []byte) error {
	err := os.Mkdir(cachePath, 0o755)
	if err != nil && !os.IsExist(err) {
		return err
	}
	return os.WriteFile(dataFile(day), data, 0o644)
}

// Get formatted filename for a given day
func dataFile(day uint) string {
	return filepath.Join(cachePath, fmt.Sprintf("day%03d.txt", day))
}
