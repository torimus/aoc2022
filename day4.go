package main

import (
	"aoc/lib"
	"strconv"
	"strings"
)

type (
	Range struct {
		from, to int
	}
)

func day4(inp *lib.Input) (cov1 int, cov2 int) {
	for _, l := range *inp {
		rngs := strings.Split(l, ",")
		ran1 := strings.Split(rngs[0], "-")
		ran2 := strings.Split(rngs[1], "-")
		rng1 := Range{
			μ(strconv.Atoi(ran1[0])),
			μ(strconv.Atoi(ran1[1]))}
		rng2 := Range{
			μ(strconv.Atoi(ran2[0])),
			μ(strconv.Atoi(ran2[1]))}
		if rng1.covers(rng2) {
			cov1++
		}
		if rng1.overlaps(rng2) {
			cov2++
		}
	}

	return cov1, cov2
}

func (r1 Range) covers(r2 Range) bool {
	return r1.from >= r2.from && r1.to <= r2.to ||
		r2.from >= r1.from && r2.to <= r1.to

}

func (r1 Range) overlaps(r2 Range) bool {
	return r1.from <= r2.to && r2.from <= r1.to
}
